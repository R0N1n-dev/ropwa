import { createApp } from "vue";
import { Inkline, components } from "@inkline/inkline";
import "@inkline/inkline/inkline.scss";
import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import App from "./App.vue";

const app = createApp(App);
app.use(Inkline, { components });
app.mount("#app");
